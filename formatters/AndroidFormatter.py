# coding: UTF-8
'''
Created on 14/06/2013

@author: Pin
'''

from itertools import izip
import os
import shutil

class AndroidFormatter(object):
    '''
    This class generates the localization files in the format expected by Android.
    e.g.
    
        values/strings.xml (default, english)
        values-pt/strings.xml
        values-es/strings.xml
        
    These files are put in an "output" folder, at the same level of the script.
    '''
    
    OUTPUT_FOLDER = "output"
    OUTPUT_FILE = "strings.xml"

    def __init__(self, string_ids_list, langs, lang_strings, empty_dummy_string):
        '''
        Params:
            - string_ids_list: list of string ids, in order of appearance. It can have "None"
             entries, which will introduce a new empty line in the generated file.
             
            - langs: list of languages to be found in lang_strings. e.g. ["EN","ES"]
             
            - lang_strings: list of lists of strings, grouped by language. e.g. [[Yes, No], [Sí, No]]
        '''
        
        self._string_ids_list = string_ids_list
        self._langs = langs
        self._lang_strings = lang_strings
        
        if empty_dummy_string:
            self._empty_dummy_string = empty_dummy_string
        else:
            self._empty_dummy_string = ""
        
        
    def generate(self):
        '''
        Generates the output files with the localizations.
        
        TODO: Params:
            - langs: list of languages to generate. If None, all languages are generated.
        '''
        
        self._create_output_folders(self._langs)
        
        for lang, lang_strings in izip(self._langs, self._lang_strings):
            
            folder_path = self.OUTPUT_FOLDER + "/values"
            
            lang_lower = lang.lower()
            if lang_lower != "en":
                folder_path = folder_path + "-" + lang_lower

            output_path = folder_path + "/" + self.OUTPUT_FILE
            
            with open(output_path, 'w+') as f:
                self._write_header(f)
                
                # write this lang's strings
                for key, value in izip(self._string_ids_list, lang_strings):

                    self._write_string(f, key, value)
                
                self._write_footer(f)
        
        
    def _create_output_folders(self, langs):
        
        # wipe anything in the previous output folder
        if (os.path.isdir(self.OUTPUT_FOLDER)):
            shutil.rmtree(self.OUTPUT_FOLDER)
        
        os.makedirs(self.OUTPUT_FOLDER)
        
        for lang in langs:
            lang_lower = lang.lower()
            
            if lang_lower == "en":
                folder = self.OUTPUT_FOLDER + "/values"
                os.makedirs(folder)
            else:
                folder = self.OUTPUT_FOLDER + "/values-" + lang_lower
                os.makedirs(folder)
        
        
    def _write_header(self, output_file):
        
        output_file.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n")
        output_file.write("<resources>\n")


    def _write_footer(self, output_file):
        
        output_file.write("</resources>")
    
    
    def _write_string(self, output_file, key, value):
        '''
        Params:
            - key: ID of the string. If None, a new line will be written. 
            - value: actual string associated to the given key.
        '''
        
        if not key:
            output_file.write("\n")
        else:
        
            text_format = "\t<string name=\"%(key)s\">%(value)s</string>"
            
            val = value
            if not val:
                val = self._empty_dummy_string
                
            text = text_format % {"key": key, "value": val}
        
            output_file.write(text.encode("UTF-8") + "\n")
        