# coding: UTF-8
'''
Created on 14/06/2013

@author: Pin
'''

from itertools import izip
import os
import shutil

class QustodioMailFormatter(object):
    '''
    This class generates the localization files in the format expected by Qustodio Mail, which uses the
    gettext format.

    http://en.wikipedia.org/wiki/Gettext
    '''

    # output path format: output/languages/en/LC_MESSAGES/qnotifications.po
    OUTPUT_FOLDER = "output"
    OUTPUT_PARENT_FOLDER_FORMAT = "languages/%(lang)s/LC_MESSAGES"
    OUTPUT_FILE = "qnotifications.po"

    def __init__(self, string_ids_list, langs, lang_strings, empty_dummy_string):
        '''
        Params:
            - string_ids_list: list of string ids, in order of appearance. It can have "None"
             entries, which will introduce a new empty line in the generated file.

            - langs: list of languages to be found in lang_strings. e.g. ["EN","ES"]

            - lang_strings: list of lists of strings, grouped by language. e.g. [[Yes, No], [Sí, No]]
        '''

        self._string_ids_list = string_ids_list
        self._langs = langs
        self._lang_strings = lang_strings

        if empty_dummy_string:
            self._empty_dummy_string = empty_dummy_string
        else:
            self._empty_dummy_string = ""


    def generate(self):
        '''
        Generates the output files with the localizations.
        '''

        self._create_output_folders(self._langs)

        for lang, lang_strings in izip(self._langs, self._lang_strings):

            folder_path = self._get_output_folder(lang)

            output_path = folder_path + "/" + self.OUTPUT_FILE

            with open(output_path, 'w+') as f:

                # write this lang's strings
                for key, value in izip(self._string_ids_list, lang_strings):

                    self._write_string(f, key, value)


    def _create_output_folders(self, langs):

        # wipe anything in the previous output folder
        if (os.path.isdir(self.OUTPUT_FOLDER)):
            shutil.rmtree(self.OUTPUT_FOLDER)

        os.makedirs(self.OUTPUT_FOLDER)

        for lang in langs:
            folder = self._get_output_folder(lang)
            os.makedirs(folder)

    def _get_output_folder(self, lang):
        return self.OUTPUT_FOLDER + "/" + self.OUTPUT_PARENT_FOLDER_FORMAT % {"lang": lang}

    def _write_string(self, output_file, key, value):
        '''
        Params:
            - key: ID of the string. If None, a new line will be written.
            - value: actual string associated to the given key.
        '''

        if not key:
            output_file.write("\n")
        else:

            val = value
            if not val:
                val = self._empty_dummy_string

            msgid_text = "msgid \"%s\"" % key
            msgstr_text = "msgstr \"%s\"" % val.replace('"', '\\"')

            output_file.write(msgid_text + "\n")
            output_file.write(msgstr_text.encode("UTF-8") + "\n\n")
