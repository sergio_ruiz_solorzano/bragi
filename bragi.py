# coding: UTF-8
'''
Created on 13/06/2013

This script parses an XLSX file with text localizations and generates the corresponding
strings.xml resources for the languages contained.

For more details, check the file description within the 'main' section or call:

bragi -h

@author: Pin-Sho Feng (pin-sho.feng@evolium.com)
'''

import argparse
import openpyxl
from formatters.AndroidFormatter import AndroidFormatter
from formatters.QustodioMailFormatter import QustodioMailFormatter

MAX_LANGUAGES = 30
MAX_STRINGS_PER_SHEET = 1500

def print_warning(string):
    print '*** WARNING: ' + string

def load_strings(filename):
    wb = openpyxl.load_workbook(filename)

    sheet = wb.get_active_sheet()

    # read all available languages
    langs = []
    for col_idx in xrange(1, MAX_LANGUAGES):
        val = sheet.cell(row=0, column=col_idx).value

        # stop when we find an empty cell
        if not val is None:
            langs.append(val)
        else:
            break

    # lang strings grouped by language and appended in order of appearance
    lang_strings = []
    for l in langs:
        lang_strings.append([])

    string_ids_set = set() # set of string IDs we've seen already (to avoid duplicates)
    string_ids_list = [] # list of string IDs in order of appearance

    sheet_names = wb.get_sheet_names()
    for name in sheet_names:
        sheet = wb.get_sheet_by_name(name)

        is_empty_row_pending = False
        for i in xrange(1, MAX_STRINGS_PER_SHEET):
            string_id = sheet.cell(row=i, column=0).value

            # skip empty values
            if not string_id:
                # add empty element so that we know there was an empty row there (will generate a newline
                # in the resources)
                is_empty_row_pending = True
                continue

            # skip duplicates
            if string_id in string_ids_set:
                print_warning("Skipping duplicate string " + string_id)
                continue

            if is_empty_row_pending:
                is_empty_row_pending = False

                string_ids_list.append(None)
                for l in lang_strings:
                    l.append(None)

            string_ids_list.append(string_id)
            string_ids_set.add(string_id)

            # read actual string values in the different languages
            for j in xrange(1, len(lang_strings) + 1):
                val = sheet.cell(row=i, column=j).value

                if not val:
                    print_warning("value for <" + string_id + "> in " + langs[j - 1] + " is empty!")

                lang_strings[j - 1].append(val)

        # add empty row between strings of different sheets
        string_ids_list.append(None)
        for l in lang_strings:
            l.append(None)

    return string_ids_list, langs, lang_strings

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=u'''\
        This script parses an XLSX file with text localizations and generates the corresponding
        strings.xml resources for the languages contained.

        The ODS file is expected to be in the following format:

        (first row/col)    EN        PT        ES
        button_ok          OK        OK        OK
        button_yes         Yes       Sim       Sí
        ...

        The languages that appear in the first row are appended to the generated resource folders.
        For the example above, the generated resources for Android would be:

        values/strings.xml (default, English)
        values-pt/strings.xml
        values-es/strings.xml

        Important notes:

        - You need openpyxl in order to run this.
        - You can have up to 30 languages (30 language columns). The language columns need to be consecutive.
          The parser will stop reading languages when an empty language cell is found.
        - You can use more than one sheet but please keep the same languages in the same order, error
          checking is limited.
        - Each sheet can have a maximum of 300 rows (this application will ignore any string passed that)
        - You can have empty rows. Each empty row is translated into a new empty line in the generated strings.xml
    ''', formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("formatter", choices=['Android', 'QustodioMail'], help="The formatter to be used to generate the localized files")
    parser.add_argument("localization_file", help="The XLSX file that contains the text localizations.")
    parser.add_argument("--empty_string_dummy", help="The string to be written when an empty value is associated to a given string key")

    args = parser.parse_args()

    string_ids_list, langs, lang_strings = load_strings(args.localization_file)

    formatter = None
    if args.formatter == 'Android':
        formatter = AndroidFormatter(string_ids_list, langs, lang_strings, args.empty_string_dummy)
    else:
        formatter = QustodioMailFormatter(string_ids_list, langs, lang_strings, args.empty_string_dummy)

    formatter.generate()

